# Copyright 2020 DALECKI Léo

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.

# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

# TUILES, Text-based User Interface fiLES, an extensible file browser library
# written in Bash

if ! (return 0 >/dev/null); then

	printf "%s error: Misuse, must be sourced\n" \
		"${BASH_SOURCE##*/}" >&2
	return 1
fi

# Private functions

# __tuiles::index_data entry
# Index data from entry, or files from $1 if tuiles_on_index hook is unset 
#	entry	Entry to index data from (files from dir by default)
# Return: 0 on success, 1 on error
__tuiles::index_data() {

	local parent="$1"
	data=() data_str=() # Flushing previously indexed data

	__tuiles::call_hook 'tuiles_on_index' && [[ -n "${data[*]}" ]] && return

	if [[ ! -d "$1" ]]; then
		tuiles::err "Cannot index files from invalid directory '$1'"
		return 1
	fi

	parent="${parent%/}"

	(( all_files == 1 )) && local files=( "$parent/."* )

	local files+=( "$parent/"* ) file str
	local -i i

	for file in "${files[@]}"; do

		str="${file##*/}" # str is the basename by default

		# Glob doesn't expand if dir is empty
		if [[ "$str" == "*" ]]; then
			tuiles::err \
				"Cannot index files from empty directory '$1'"
			return 1
		fi

		# The hook handled the file's indexing
		if __tuiles::call_hook 'tuiles_filter_file'; then

			data[$i]="$file"
			data_str[$i]="$str"
		fi

		(( i++ ))
	done

	return 0 # Incrementation will return code 1 if it evals to 0
}

# __tuiles::is_hooked hook [var]
# Check if hook is set and stores its name in var
# 	hook	Hook to check for
#	var	Var to store hook's name
# Return: 0 if hooked, 1 otherwise
__tuiles::is_hooked() {

	local local_hook="${1,,}" \
		user_hook="${1^^}" \
		ret_var

	# If a return var is given, use a nameref
	[[ -n $2 ]] && local -n ret_var="$2"

	if declare -F "$local_hook" >/dev/null; then
		ret_var="$local_hook"
		return 0
	elif (( user_hooks == 1 )) \
		&& declare -F "$user_hook" >/dev/null; then
		
		ret_var="$user_hook"
		return 0
	fi

	return 1
}

# __tuiles::call_hook hook
# Call hooked function
#	hook	Called hook 
# Return: Hooked function's return code, 0 if none hooked
__tuiles::call_hook() {

	local hook

	if __tuiles::is_hooked "$1" hook; then
		$hook
		return
	fi

	return 0 # Hooks are optional, return success if none are set
}

# __tuiles::open entry
# Index and then browse entry
#	entry	Entry to open
# Return: 0 on success, 1 on error
__tuiles::open() {

	local -a data data_str

	__tuiles::index_data "$1" || return 1
	
	local -i entry_i page page_first_i page_last_i page_count
	local entry="$data" cur_parent="$1"

	if (( bottomed == 1 )); then # Reset bottoming

		bottomed=
		local -i i

		# Try to select previous parent
		while [[ -n "$prev_parent" && -n "${data[$i]}" ]]; do
			
			if [[ "${data[$i]}" == "$prev_parent" ]]; then

				local -i entry_i=$i; break
			fi

			(( i++ ))
		done
	else
		recur_lvl+=1
	fi

	tuiles::draw_pg || return 1 # Redrawing current dir

	local key_seq

	# Reading terminal one char at a time
	while (( stop_recur != -1 )) && read -rsN 1 char; do

		key_seq+="$char" # Appending last char to the sequence

		# If hook handled the sequence, reset it
		__tuiles::call_hook 'tuiles_on_keystroke' && key_seq=

		if (( stop_recur > 0 )); then

			if (( recur_lvl == 1 )); then # Bottoming out

				stop_recur=0 bottomed=1
				local prev_parent="$cur_parent"

				# Kill this recursion when bottoming handled
				__tuiles::call_hook 'tuiles_on_bottom' && {

					(( recur_lvl-- ))
					return 0
				}
			else
				(( stop_recur-- )); (( recur_lvl-- ))
				return 0
			fi
		elif (( stop_recur == -1 )); then
			return 0
		fi
	done
}

# __tuiles::init_traps
# Set traps
__tuiles::init_traps() {

	traps_bck="$(trap)"

	local t

	for t in "${!traps_bck[@]}"; do
		traps_bck[$t]="$(trap -p $t)"
	done

	trap 'tuiles::quit; return' INT TERM
	trap 'tuiles::draw_pg' WINCH # On window resize, redraw page
}

# __tuiles::reset_traps
# Restore backed-up traps
__tuiles::reset_traps() {

	local t cmd

	for t in "${!traps_bck[@]}"; do

		cmd="${traps_bck[$t]}"

		if [[ -n "$cmd" ]]; then
			eval "$cmd"
		else
			trap - $t
		fi
	done
}

# __tuiles::init_term
# Init terminal
__tuiles::init_term() {

	term_init=1

	tput smcup # Dump term to mem
	tput civis
}

# __tuiles::reset_term
# Restore terminal from memory
__tuiles::reset_term() {

	if (( term_init == 1 )); then
		tput rmcup # Restore term from mem
		tput cnorm
	fi
}

# __tuiles::print_err msg
# Print error message to stderr
#	msg	Message to print
__tuiles::print_err() {

	printf '%s error: %s\n' "${BASH_SOURCE##*/}" "$1" >&2
}

# Public functions

# tuiles::quit [code]
# Quit TUILES with given exit code
#	code	Exit code (0 by default)
tuiles::quit() {
	
	__tuiles::call_hook 'tuiles_on_quit'
	
	__tuiles::reset_term
	__tuiles::reset_traps

	(( hard_exit == 1 )) && exit "${1:-0}"

	quit=1
	stop_recur=-1
	exit_code="${1:-0}"
	return 0
}

# tuiles::err msg [code]
# Set TUILES to exit with error
#	msg	Error message
#	code	Exit code
tuiles::err() {
	
	if (( hard_exit == 1 )); then
		__tuiles::reset_term
		__tuiles::print_err "$1"
		exit "${2:-1}"
	fi

	err_msg="$1"
	tuiles::quit "${2:-1}"
	return 0
}

# tuiles::draw_pg [page]
# Draw page
#	page	Page to draw (current page by default)
# Return: 0 on success, 1 on error
tuiles::draw_pg() {

	(( stop_recur == -1 )) && return 0

	term_cols=$(tput cols)
	local -i term_lines=$(tput lines)
	page_count=$(( ( ${#data[*]} - 1 ) / term_lines ))

	if [[ ! "$1" =~ ^[[:digit:]]*$ ]]; then
		tuiles::err "Invalid page number '$1'"; return
		return 1
	elif [[ -n "$1" ]] && (( $1 < 0 || $1 > page_count )); then
		tuiles::err "Page '$1' out of bounds"; return
		return 1
	fi

	# Set page to the given one or stay on the same, then set the index of
	# the first and last entry of the page
	local -i cur_page=$(( entry_i / term_lines ))
	page=${1:-$cur_page}
	page_first_i=$(( page * term_lines ))
	page_last_i=$(( page_first_i + term_lines - 1 ))

	# Page's last index shouldn't go beyond the data's last one
	(( page_last_i >= ${#data[*]} )) && page_last_i=$(( ${#data[*]} - 1 ))

	# Move the selection to this page and ensure it can't go out of it
	(( entry_i / term_lines != page )) \
		&& entry_i=$(( entry_i % term_lines + page_first_i )) 
	(( entry_i > page_last_i )) && entry_i="$page_last_i"

	entry="${data[$entry_i]}"

	clear
	tput home

	local -i i cur_line
	local print_str

	for (( i = page_first_i ; i <= page_last_i ; i++ )); do

		print_str="${data_str[$i]:0:$term_cols}" # No line warp

		# If we're drawing the selection, highlight it and make it the
		# cursor's position
		if (( i == entry_i )); then

			printf '%s' "$smso$print_str$sgr0"

			# Cursor's position is relative to the current page,
			# hence the modulo
			cur_line=$(( i % term_lines )) 
		else

			printf '%s' "$print_str"
		fi

		# Print newline if its not the last line
		(( i != page_last_i )) && printf '\n'
	done

	tput cup "$cur_line" 0
}

# tuiles::flip_pg count
# Flip given amount of page
#	count	Pages to flip, goes backward if negative
# Return: 0 on success, 1 on error
tuiles::flip_pg() {

	if [[ ! "$1" =~ ^-?[[:digit:]]+$ ]]; then
		tuiles::err "Invalid integer '$1'"; return
		return 1
	fi

	local -i target_page=$(( page + $1 ))

	(( target_page < 0 || target_page > page_count )) && return 1

	tuiles::draw_pg $(( page + $1 ))
}

# tuiles::move count
# Move cursor given count
#	count	Movements, goes up if negative
tuiles::move() {
	
	(( $1 == 0 )) && return 0

	local -i i=$1

	while (( i != 0 )); do # While there still are movements to be done

		# Redraw current line
		printf '%s\r' "${data_str[$entry_i]:0:$term_cols}"

		if (( i > 0 )); then # i is positive, we're moving down
			(( entry_i == ${#data[@]} - 1 )) && break

			(( i-- )); (( entry_i++ ))

			# If we moved out of the page, flip it
			(( entry_i > page_last_i )) \
				&& tuiles::flip_pg 1 || tput cud1

		else # Otherwise i is negative, we're moving up
			(( entry_i == 0 )) && break
			
			(( i++ )); (( entry_i-- ))

			(( entry_i < page_first_i )) \
				&& tuiles::flip_pg -1 || tput cuu1
		fi
	done

	entry="${data[$entry_i]}"

	# Redraw selected line
	printf '%s\r' "$smso${data_str[$entry_i]:0:$term_cols}$sgr0"
}

# tuiles::open [entry]
# Open entry
#	entry	Entry to browse (selected one by default)
# Return: 1 if entry cannot be opened with default indexing, 0 othewise
tuiles::open() {
	
	local target="${1:-$entry}"
	local content=( "${target%/}"/* )

	# Don't open if using default indexing and dir is empty
	! __tuiles::is_hooked 'tuiles_on_index' \
		&& [[ "${content##*/}" == "*" ]] \
		&& return 1

	__tuiles::open "$target"
	tuiles::draw_pg
}

# tuiles::back [count]
# Go back
#	count	Amount of times to go back (1 by default)
tuiles::back() {

	stop_recur="${1:-1}"
}

# tuiles::refresh
# Re-index data and redraw current page
tuiles::refresh() {
	
	__tuiles::index_data "$cur_parent"
	tuiles::draw_pg

	entry="${data[$entry_i]}"
}

# tuiles::invoke [-eu] root
# Invoke TUILES at given root
#	root	Indexing root
# Options:
#	-a	All, default indexing will show hidden files
#	-e	Hard exit, TUILES will call exit when quitting
#	-u	User hooks, enable user hooks
# Return: 0 on success, 1 or above if an error occured
tuiles::invoke() {

	local -i stop_recur recur_lvl bottomed user_hooks hard_exit exit_code \
		quit term_init term_cols all_files bottom_out
	local smso="$(tput smso)" sgr0="$(tput sgr0)" err_msg
	local -A traps_bck=([INT]='' [TERM]='' [WINCH]='')

	while getopts 'aeu' opt; do
		case "$opt" in
			a) all_files=1 ;;
			e) hard_exit=1 ;;
			u) user_hooks=1
		esac
	done

	shift $(( OPTIND - 1 ))
	OPTIND=1 # Reset to avoid wrong shift on next invocation 

	if ! __tuiles::is_hooked 'tuiles_on_keystroke'; then

		tuiles_on_keystroke() { # Default key stroke hook
			
			case "$key_seq" in
				*$'\n')
					tuiles::open ;;
				*$'\x7f')
					tuiles::back ;;
				*q)
					tuiles::quit ;;
				*c)
					cd "$cur_parent"
					tuiles::quit ;;
				*[[:cntrl:]][A)
					tuiles::move -1 ;;
				*[[:cntrl:]][B)
					tuiles::move 1 ;;
				*[[:cntrl:]][C)
					tuiles::flip_pg 1 ;;
				*[[:cntrl:]][D)
					tuiles::flip_pg -1 ;; # Coloring fix ]
				*)
					return 1
			esac

			return 0
		}
	fi

	if ! __tuiles::is_hooked 'tuiles_on_index' \
		&& ! __tuiles::is_hooked 'tuiles_on_bottom'; then

		tuiles_on_bottom() { # Default bottoming hook

			local new_parent="$(realpath $cur_parent/..)" \
				prev_parent="$(realpath $cur_parent)"

			# Don't allow backing when on /
			[[ "$new_parent" == "$cur_parent" ]] && return 1

			tuiles::open "$new_parent"
		}
	fi

	__tuiles::init_traps
	__tuiles::init_term
	__tuiles::open "$1"

	(( quit != 1 )) && tuiles::quit

	if [[ -v err_msg ]]; then
		__tuiles::print_err "$err_msg"
	fi

	return "$exit_code"
}
