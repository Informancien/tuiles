# TUILES

Text-based User Interface fiLES, an extensible file browser library written in
Bash.

![TUILES demo](tuiles_demo.gif)

## Usage

This library is meant to be sourced by a script or an interactive shell to
extend its functionality. When sourced, the `tuiles::invoke` function can be
called to invoke the browser:

	. tuiles.bash # Sourcing TUILES tuiles::invoke "$HOME" # Invoking
	browser with root $HOME

### Invocation

The `tuiles::invoke` function takes one argument, the root (the bottom-most
element) of the tree structure to explore, by default the local filesystem (see
the `tuiles_on_index` hook bellow). 

The following options can also be provided:
* `-a`: **A**ll, display hidden files when using the default indexing
  algorithm;
* `-e`: Hard **E**xit, will cause TUILES to call `exit` when quitting, will
  cause the current shell to die;
* `-u`: **U**ser hooks, enables the user hooks (see **Extensibility** bellow). 

## Naming convention

To avoid conflicts with the sourcing script or shell, the pseudo namespace
`tuiles` is used to prefix functions declared within `tuiles.bash`.  Functions
prefixed with two underscores `__` are considered private: they shouldn't be
called outside `tuiles.bash`. Other functions, aside from `tuiles::invoke`, are
meant to be called within hooks.

## Bottoming out

Recursion is used to browse the given tree, as it enables going back extremly
quickly. But it means that backing away from the invocation root (the argument
passed to `tuiles::invoke`) will end the execution as the initial recursion
dies: this will be referred as _bottom out_ of the tree.

The `tuiles_on_bottom` hook handles this event, which might be necessary when
the invocation root isn't the real root of your tree. For example, if you
browse the local filesystem and invoke TUILES at `/bin`, which isn't the tree's
real root `/`.

## Extensibility

If a function has the name of a hook in upper or lower case, it will be called
on certain events. A hook that handled succesfully an event should **always**
return the code `0`, any other code can be returned in case of failure.

When a hook is called, some variables might be declared by the caller that can
be referenced or set by the hook.

For the hooked function declared in upper case to be called, the `-u` option
must be provided to `tuiles::invoke`. Even with this option enabled, hooked
functions declared in lower case take precedence over them, so that
`TUILES_ON_KEYSTROKE` will only be called if the `-u` option is used, and no
function called `tuiles_on_keystroke` is declared. This behavior allows allows
for the user to define hooks, that can be used by a script (this was inspired
by the way the `readline` library always uses the same user configuration,
providing a similar behavior across programs).
 
### Hooks

Hook | Event | Context
--- | --- | ---
`tuiles_on_keystroke` | Key press | `$key_seq` contains the last keystrokes, `$entry` is the currently selected entry's data and `$cur_parent` is the current location.
`tuiles_on_quit` | Before quitting the browser |
`tuiles_on_index` | Provides an indexing algorithm instead of the default one | Needs to populate the `data` and `data_str` arrays, where `${data[0]}` is the first indexed element, and `${data_str[0]}` is the first element's display string in the menu.
`tuiles_filter_file` | Filters the files that should be indexed by the default indexing algorithm | `$file` expands to the currently filtered file's path, `str` should be set to be the file's display string in the menu (its basename by default). Needs to return `0` if the file must be indexed, `1` otherwise.
`tuiles_on_bottom` | Backing from invocation root | Will handle calls to `tuiles::back` when browsing invocation root (first argument passed to `tuiles::invoke`). `$cur_parent` expands to what we're backing from (current directory for example), `prev_parent` can be set to which new entry `$cur_parent` should match if calling `tuiles::open`. Return `0` if backing is handled, `1` otherwise.

## License

TUILES is developped by DALECKI Léo and provided under the GNU Lesser General
Public License (see the files [COPYING](COPYING) and
[COPYING.LESSER](COPYING.LESSER)). You are required to provide any published
modification of TUILES under a compatible license, but you're allowed to use it
as a library from proprietary software.
